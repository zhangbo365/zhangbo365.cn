package cn.zhangbo365.service;

import cn.zhangbo365.dao.LoginMapper;
import cn.zhangbo365.pojo.Login;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.SimpleDateFormat;

import static org.junit.Assert.*;

public class UserServiceTest {
    ApplicationContext ctx=null;
    @Before
    public void setUp() throws Exception {
        ctx = new ClassPathXmlApplicationContext("applicationContext-mybatis.xml");
    }

    @Test
    public void zbLogin() throws Exception {
//        UserService userService = (UserService) ctx.getBean("userService");

        Login login=new Login();
        login.setUserid("123");
        login.setUserPass("123");
        login.setBirthdate(new SimpleDateFormat("yyyy-MM-dd").parse("2011-1-1"));

        LoginMapper loginMapper = (LoginMapper) ctx.getBean("loginMapper");
        System.out.println(loginMapper.addUser(login));
    }

}