$(function () {
    $('#loginForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {/*输入框不同状态，显示图片的样式*/
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {/*验证*/
            userid: {/*键名和input name值对应*/
                message: 'The username is not valid',
                validators: {
                    notEmpty: {/*非空提示*/
                        message: '用户名不能为空'
                    }
                }
            },
            userPass: {
                message:'密码无效',
                validators: {
                    notEmpty: {
                        message: '密码不能为空'
                    },
                    stringLength: {
                        min: 4,
                        message: '密码长度必须大于等于4个字符'
                    }
                }
            }
        }
    });
    $("#loginForm").submit(function () {
        $("#loginError").html("");
        $(":submit",this).addClass("disabled");
        $.ajax({
            url:"/doLogin.do",
            data:$(this).serialize(),
            type:"post",
            dataType:"json",
            success:function (result) {
                $("#loginForm :submit").removeClass("disabled");
                if(result.userid){
                    window.location.reload();
                }else{
                    $("#loginError").html("用户名或密码错误")
                }
            }
        });
        return false;
    });

    $("#logout").click(function () {
        $.get("/logout.do",function () {
            window.location.reload();
        });
    });

    $('#registerForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {/*输入框不同状态，显示图片的样式*/
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {/*验证*/
            userid: {/*键名和input name值对应*/
                message: 'The username is not valid',
                validators: {
                    notEmpty: {/*非空提示*/
                        message: '用户名不能为空'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_]+$/,
                        message: '只能输入英文、数字和下划线‘_’'
                    },
                    remote: {
                        url: '/checkUserExists.do',
                        message:"用户已存在",
                        type: "get",
                        dataType: 'json',
                        delay: 1500
                    }
                }
            },
            userPass: {
                message:'密码无效',
                validators: {
                    notEmpty: {
                        message: '密码不能为空'
                    },
                    identical: {
                        field: 'userRePass',
                        message: '密码与确认密码不同'
                    },
                    stringLength: {
                        min: 4,
                        message: '密码长度必须大于等于4个字符'
                    }
                }
            },
            userRePass: {
                message:'重复密码无效',
                validators: {
                    notEmpty: {
                        message: '确认密码不能为空'
                    },
                    identical: {
                        field: 'userPass',
                        message: '密码与确认密码不同'
                    },
                    stringLength: {
                        min: 4,
                        message: '确认密码长度必须大于等于4个字符'
                    }
                }
            }
        }
    });
})