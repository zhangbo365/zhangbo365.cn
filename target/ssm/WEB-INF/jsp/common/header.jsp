<%--
  Created by IntelliJ IDEA.
  User: Zane
  Date: 2017/9/1
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>老鬼酱</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/statics/img/favicon.ico" rel="icon" type="image/x-icon" />
    <link rel="stylesheet" href="statics/css/bootstrap.min.css">
    <link rel="stylesheet" href="statics/css/bootstrapValidator.min.css">
    <link rel="stylesheet" href="statics/css/index.css">
    <script src="statics/js/jquery-1.12.4.js"></script>
    <script src="statics/js/bootstrap.min.js"></script>
    <script src="statics/js/bootstrapValidator.min.js"></script>
    <script src="statics/js/common.js"></script>
</head>
<body>
<div id="loginmodal" class="modal fade">
    <div class="modal-dialog" style="margin-top: 100px;">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="padding:0 20px;">登陆</h4>
            </div>
            <div class="media-body" style="padding-top:15px">
                <form id="loginForm" style="padding: 0 30px;">
                    <div class="form-group">
                        <input type="text" name="userid" class="form-control" placeholder="用户名">
                    </div>
                    <div class="form-group">
                        <input type="password" name="userPass" class="form-control" placeholder="密码">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary form-control" >
                    </div>
                </form>
            </div>
            <div class="modal-footer"><span class="text-danger" style="padding-right: 20px" id="loginError"></span>CopyRight&copy;老鬼酱 2017</div>
        </div>
    </div>
</div>

<nav class="nav navbar-inverse ">
    <button  class="navbar-toggle" data-toggle="collapse"
             data-target=".navbar-collapse" >
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse navbar-left">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="padding: 0 10px;">
            <div class="navbar-header">
                <a href="/" class="navbar-brand">LOGO</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <c:if test="${not empty user_session}">
                    <li><a href='#' readonly>欢迎，${user_session}</a></li>
                    <li><a href='#' id="logout">注销</a></li>
                </c:if>
                <c:if test="${empty user_session}">
                    <li><a href="#loginmodal" data-toggle="modal">登陆</a></li>
                    <li><a href="/register.do">注册</a></li>
                </c:if>
                <li>
                    <form action="#" class="navbar-form" role="search" method="post" id="searchForm">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="搜索">
                        </div>
                        <input type="submit" class="btn btn-primary col-sx-12">
                    </form>
                </li>
            </ul>
        </nav>
    </div>
</nav>