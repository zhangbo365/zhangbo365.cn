<%--
  Created by IntelliJ IDEA.
  User: Zane
  Date: 2017/9/1
  Time: 14:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common/header.jsp"%>
<div class="container">
    <div class="row">
        <div class="col-lg-7 col-xs-12">
            <div style="text-align: center;padding: 10px;">
                <img src="/statics/img/register_l.jpg" alt="Register">
            </div>
        </div>
        <div class="col-lg-5 col-xs-12">
            <form id="registerForm" action="/doRegister.do" style="padding-top: 30px;">
                <div class="form-group">
                    <input type="text" name="userid" class="form-control" placeholder="用户名" autocomplete="off">
                </div>
                <div class="form-group">
                    <input type="password" name="userPass" class="form-control" placeholder="密码">
                </div>
                <div class="form-group">
                    <input type="password" name="userRePass" class="form-control" placeholder="确认密码">
                </div>
                <div class="form-group">
                    <span class="alert-warning">您的生日默认为（0000-00-00），删除角色时会用到哦！</span>
                </div>
                <div class="form-group">
                    <input type="submit" value="注册" class="btn btn-primary form-control" >
                </div>
            </form>
        </div>
    </div>
<%@include file="common/footer.jsp"%>
