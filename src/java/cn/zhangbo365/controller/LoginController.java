package cn.zhangbo365.controller;

import cn.zhangbo365.pojo.Login;
import cn.zhangbo365.service.UserService;
import cn.zhangbo365.tools.Constants;
import com.alibaba.fastjson.JSON;
import com.mysql.jdbc.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.rmi.runtime.Log;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class LoginController {
    @Resource
    private UserService userService;

    @RequestMapping(value = "/doLogin.do",method = RequestMethod.POST)
    @ResponseBody
    public Object doLogin(String userid, String userPass, HttpSession session) {
        if(!StringUtils.isNullOrEmpty(userid)){
            Login login=userService.zbLogin(userid,userPass);
            if(login!=null){
                session.setAttribute(Constants.USER_SESSION, userid);
                return login;
            }else {
                return "{\"userid\":null}";
            }
        }else{
            return "{\"userid\":null}";
        }
    }

    @RequestMapping(value = "/logout.do")
    @ResponseBody
    public Object logout(HttpSession session){
        session.removeAttribute(Constants.USER_SESSION);
        return "1";
    }

    @RequestMapping(value = "/register.do")
    public String register(){
        return "userRegister";
    }

    @RequestMapping(value = "/doRegister.do")
    public String doRegister(Login login, HttpSession session, HttpServletResponse response) throws IOException {
        if(userService.zbRegister(login)){
            session.setAttribute(Constants.USER_SESSION, login.getUserid());
            return "redirect:/index.jsp";
        }else{
            return "userRegister";
        }
    }

    @RequestMapping(value = "/checkUserExists.do")
    @ResponseBody
    public Object checkUserExists(String userid){
        Map<String,String> map=new HashMap<String, String>();
        Boolean exists=userService.checkUserExists(userid);
        map.put("message",exists?"用户名已被使用":"用户名可以使用");
        map.put("valid",exists?"false":"true");
        return map;
    }
}
