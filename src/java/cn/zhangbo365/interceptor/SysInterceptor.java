package cn.zhangbo365.interceptor;

import cn.zhangbo365.tools.Constants;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SysInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if(request.getSession().getAttribute(Constants.USER_SESSION)!=null){
            return true;
        }else{
            response.sendRedirect(request.getContextPath()+"/401.jsp");
            return false;
        }

    }
}
