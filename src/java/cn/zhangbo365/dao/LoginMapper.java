package cn.zhangbo365.dao;

import cn.zhangbo365.pojo.Login;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LoginMapper {
    /**
     * 根据用户id得到用户对象
     * @param userid 用户id
     * @return 根据用户id得到的用户对象
     */
    Login getByUserid(String userid);

    /**
     * 保存用户
     * @param login 用户对象
     * @return null：保存失败；非空对象：保存成功
     */
    int addUser(Login login);
}