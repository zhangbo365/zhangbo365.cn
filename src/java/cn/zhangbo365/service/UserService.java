package cn.zhangbo365.service;

import cn.zhangbo365.pojo.Login;
/**
 * 应为事务AOP必须以zb开头（在spring中配置的），所以业务逻辑中的方法也必须以zb开头
 */
public interface UserService {
    /**
     * 登陆业务逻辑
     * @param userid 用户id
     * @param userPass 用户密码
     * @return null：登陆失败 ；login对象：登陆成功
     */
    Login zbLogin(String userid,String userPass);

    /**
     * 注册业务：检查用户名是否已存在
     * @param userid 用户名
     * @return true：已存在；false：不存在
     */
    Boolean checkUserExists(String userid);

    /**
     * 注册业务逻辑
     * @param login 注册用户
     * @return true：注册成功；false：注册失败
     */
    boolean zbRegister(Login login);
}
