package cn.zhangbo365.service;

import cn.zhangbo365.dao.LoginMapper;
import cn.zhangbo365.pojo.Login;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Resource
    private LoginMapper loginMapper;
    public Login zbLogin(String userid, String userPass) {
        Login user=null;

        user=loginMapper.getByUserid(userid);

        if(user!=null){
            if(userPass!=null && user.getUserPass().equals(userPass)){
                return user;
            }
        }
        //如果用户不存在或密码错误，返回null
        return null;
    }

    public Boolean checkUserExists(String userid) {
        return loginMapper.getByUserid(userid)!=null?true:false;
    }

    public boolean zbRegister(Login login) {
        if(loginMapper.addUser(login)==1){return true;}
        else{return false;}
    }
}
